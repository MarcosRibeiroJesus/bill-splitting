package controllers

import (
	"encoding/json"
	"io"
	"net/http"
)

func RegisterControllers() {
	bc := newBillController()
	uc := newUserController()
	oc := newOrderController()
	pc := newProductController()

	http.Handle("/bills", *bc)
	http.Handle("/bills/", *bc)
	http.Handle("/users", *uc)
	http.Handle("/users/", *uc)
	http.Handle("/orders", *oc)
	http.Handle("/orders/", *oc)
	http.Handle("/products", *pc)
	http.Handle("/products/", *pc)
}

func encodeResponseAsJSON(data interface{}, w io.Writer) {
	enc := json.NewEncoder(w)
	enc.Encode(data)
}
