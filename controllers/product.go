package controllers

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strconv"

	"github.com/MarcosRibeiroJesus/Bill-Splitting/models"
	validator "github.com/MarcosRibeiroJesus/Bill-Splitting/validators"
)

type productController struct {
	productIDPattern *regexp.Regexp
}

func (pc productController) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/products" {
		switch r.Method {
		case http.MethodGet:
			pc.getAll(w, r)
		case http.MethodPost:
			pc.post(w, r)
		default:
			w.WriteHeader(http.StatusNotImplemented)
		}
	} else {
		matches := pc.productIDPattern.FindStringSubmatch(r.URL.Path)
		if len(matches) == 0 {
			w.WriteHeader(http.StatusNotFound)
		}
		id, err := strconv.Atoi(matches[1])
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
		}
		switch r.Method {
		case http.MethodGet:
			pc.get(id, w)
		case http.MethodPut:
			pc.put(id, w, r)
		case http.MethodDelete:
			pc.delete(id, w)
		default:
			w.WriteHeader(http.StatusNotImplemented)
		}
	}
}

func (pc *productController) getAll(w http.ResponseWriter, r *http.Request) {
	encodeResponseAsJSON(models.GetProducts(), w)
}

func (pc *productController) get(id int, w http.ResponseWriter) {
	p, err := models.GetProductByID(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	encodeResponseAsJSON(p, w)
}

func (pc *productController) post(w http.ResponseWriter, r *http.Request) {
	p, err := pc.parseRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Could not parse Product object"))
		return
	}
	err = validator.ValidateStruct(p)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	p, err = models.AddProduct(p)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	encodeResponseAsJSON(p, w)
}

func (pc *productController) put(id int, w http.ResponseWriter, r *http.Request) {
	p, err := pc.parseRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Could not parse Product object"))
		return
	}
	if id != p.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("ID of submitted product must match ID in URL"))
		return
	}
	p, err = models.UpdateProduct(p)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	encodeResponseAsJSON(p, w)
}

func (pc *productController) delete(id int, w http.ResponseWriter) {
	err := models.RemoveProductByID(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (pc *productController) parseRequest(r *http.Request) (models.Product, error) {
	dec := json.NewDecoder(r.Body)
	var product models.Product
	err := dec.Decode(&product)
	if err != nil {
		return models.Product{}, err
	}
	return product, nil
}

func newProductController() *productController {
	return &productController{
		productIDPattern: regexp.MustCompile(`^/products/(\d+)/?`),
	}
}
