package controllers

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strconv"

	"github.com/MarcosRibeiroJesus/Bill-Splitting/models"
	validator "github.com/MarcosRibeiroJesus/Bill-Splitting/validators"
)

type orderController struct {
	orderIDPattern *regexp.Regexp
}

func (oc orderController) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/orders" {
		switch r.Method {
		case http.MethodGet:
			oc.getAll(w, r)
		case http.MethodPost:
			oc.post(w, r)
		default:
			w.WriteHeader(http.StatusNotImplemented)
		}
	} else {
		matches := oc.orderIDPattern.FindStringSubmatch(r.URL.Path)
		if len(matches) == 0 {
			w.WriteHeader(http.StatusNotFound)
		}
		id, err := strconv.Atoi(matches[1])
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
		}
		switch r.Method {
		case http.MethodGet:
			oc.get(id, w)
		case http.MethodPut:
			oc.put(id, w, r)
		case http.MethodDelete:
			oc.delete(id, w)
		default:
			w.WriteHeader(http.StatusNotImplemented)
		}
	}
}

func (oc *orderController) getAll(w http.ResponseWriter, r *http.Request) {
	encodeResponseAsJSON(models.GetOrders(), w)
}

func (oc *orderController) get(id int, w http.ResponseWriter) {
	o, err := models.GetOrderByID(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}
	encodeResponseAsJSON(o, w)
}

func (oc *orderController) post(w http.ResponseWriter, r *http.Request) {
	o, err := oc.parseRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Could not parse Order object"))
		return
	}
	err = validator.ValidateStruct(o)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	o, err = models.AddOrder(o)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	encodeResponseAsJSON(o, w)
}

func (oc *orderController) put(id int, w http.ResponseWriter, r *http.Request) {
	o, err := oc.parseRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Could not parse Order object"))
		return
	}
	if id != o.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("ID of submitted order must match ID in URL"))
		return
	}
	o, err = models.UpdateOrder(o)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	encodeResponseAsJSON(o, w)
}

func (oc *orderController) delete(id int, w http.ResponseWriter) {
	err := models.RemoveOrderByID(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (oc *orderController) parseRequest(r *http.Request) (models.Order, error) {
	dec := json.NewDecoder(r.Body)
	var order models.Order
	err := dec.Decode(&order)
	if err != nil {
		return models.Order{}, err
	}
	return order, nil
}

func newOrderController() *orderController {
	return &orderController{
		orderIDPattern: regexp.MustCompile(`^/orders/(\d+)/?`),
	}
}
