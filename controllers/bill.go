package controllers

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strconv"

	"github.com/MarcosRibeiroJesus/Bill-Splitting/models"
)

type billController struct {
	billIDPattern *regexp.Regexp
}

func (bc billController) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/bills" {
		switch r.Method {
		case http.MethodGet:
			bc.getAll(w, r)
		case http.MethodPost:
			bc.post(w, r)
		default:
			w.WriteHeader(http.StatusNotImplemented)
		}
	} else {
		matches := bc.billIDPattern.FindStringSubmatch(r.URL.Path)
		if len(matches) == 0 {
			w.WriteHeader(http.StatusNotFound)
		}
		id, err := strconv.Atoi(matches[1])
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
		}
		switch r.Method {
		case http.MethodGet:
			bc.get(id, w)
		case http.MethodPut:
			bc.put(id, w, r)
		case http.MethodDelete:
			bc.delete(id, w)
		default:
			w.WriteHeader(http.StatusNotImplemented)
		}
	}
}

func (bc *billController) getAll(w http.ResponseWriter, r *http.Request) {
	encodeResponseAsJSON(models.GetBills(), w)
}

func (bc *billController) get(id int, w http.ResponseWriter) {
	b, err := models.GetBillByID(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	encodeResponseAsJSON(b, w)
}

func (bc *billController) post(w http.ResponseWriter, r *http.Request) {
	b, err := bc.parseRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Could not parse Bill object"))
		return
	}
	b, err = models.AddBill(b)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	encodeResponseAsJSON(b, w)
}

func (bc *billController) put(id int, w http.ResponseWriter, r *http.Request) {
	b, err := bc.parseRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Could not parse Bill object"))
		return
	}
	if id != b.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("ID of submitted bill must match ID in URL"))
		return
	}
	b, err = models.UpdateBill(b)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	encodeResponseAsJSON(b, w)
}

func (bc *billController) delete(id int, w http.ResponseWriter) {
	err := models.RemoveBillByID(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (bc *billController) parseRequest(r *http.Request) (models.Bill, error) {
	dec := json.NewDecoder(r.Body)
	var bill models.Bill
	err := dec.Decode(&bill)
	if err != nil {
		return models.Bill{}, err
	}
	return bill, nil
}

func newBillController() *billController {
	return &billController{
		billIDPattern: regexp.MustCompile(`^/bills/(\d+)/?`),
	}
}
