module github.com/MarcosRibeiroJesus/Bill-Splitting

go 1.13

require (
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/leodido/go-urn v1.2.1 // indirect
)
