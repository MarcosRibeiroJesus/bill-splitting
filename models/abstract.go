package models

import (
	"errors"
	"fmt"
	"net/smtp"
	"strconv"
)

func SumTotal(value, quantity int64) int64 {
	return quantity * value
}

func GetRemainder(bill Bill) int64 {
	return bill.Total % int64(len(bill.SplitBetween))
}

func Divide(bill Bill) (quotient int64, err error) {
	if len(bill.SplitBetween) == 0 {
		err = errors.New("cannot divide by zero")
	}

	quotient = bill.Total / int64(len(bill.SplitBetween))
	return
}

func SplitBetweenBuddies(b Bill, quotient int64) Bill {
	fmt.Println("quotient :", quotient)
	for i, _ := range b.SplitBetween {
		b.SplitBetween[i].ShouldPay = quotient
	}
	return b
}

func SendEmail(buddy Buddy) {
	from := "stone.ton.2021@gmail.com"
	password := "ulzyuqvchevblzyh"

	to := []string{
		buddy.Email,
	}

	smtpHost := "smtp.gmail.com"
	smtpPort := "587"

	// Message.
	message := []byte("Subject: discount Gophers!\r\n" + "Hello Buddy! You have to pay for the bill just " + strconv.FormatInt(buddy.ShouldPay, 10))

	// Authentication.
	auth := smtp.PlainAuth("", from, password, smtpHost)

	// Sending email.
	err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	if err != nil {
		fmt.Println(err)
		return
	}
}
