package models

import (
	"errors"
	"fmt"
)

var (
	orders      []*Order
	nextOrderID = 1
)

func GetOrders() []*Order {
	return orders
}

func AddOrder(o Order) (Order, error) {
	if o.ID != 0 {
		return Order{}, errors.New("New Order must not include id or it must be set to zero")
	}
	o.ID = nextOrderID

	if o.Quantity > 1 {
		o.Total = SumTotal(o.Product.Price, o.Quantity)
	} else {
		o.Total = o.Product.Price
	}

	_, err := AddItemToBill(o, o.BillID)
	if err != nil {
		return Order{}, err
	}

	nextOrderID++
	orders = append(orders, &o)

	return o, nil
}

func GetOrderByID(id int) (Order, error) {
	for _, o := range orders {
		if o.ID == id {
			return *o, nil
		}
	}

	return Order{}, fmt.Errorf("Order with ID '%v' not found", id)
}

func UpdateOrder(o Order) (Order, error) {
	for i, candidate := range orders {
		if candidate.ID == o.ID {
			orders[i] = &o
			return o, nil
		}
	}

	return Order{}, fmt.Errorf("Order with ID '%v' not found", o.ID)
}

func RemoveOrderByID(id int) error {
	for i, o := range orders {
		if o.ID == id {
			orders = append(orders[:i], orders[i+1:]...)
			return nil
		}
	}

	return fmt.Errorf("Order with ID '%v' not found", id)
}
