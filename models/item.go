package models

type Item struct {
	ID          int    `json:"id"`
	ProductName string `json:"product_name" validate:"required"`
	Price       int64  `json:"price" validate:"gte=0,required"`
	Total       int64  `json:"total"`
	Quantity    int64  `json:"quantity" validate:"gte=1,required"`
}
