package models

import (
	"errors"
	"fmt"
	"math/rand"
)

var (
	bills      []*Bill
	nextBillID = 1
	quotient   int64
)

func GetBills() []*Bill {
	return bills
}

func AddBill(b Bill) (Bill, error) {
	if b.ID != 0 {
		return Bill{}, errors.New("New Bill must not include id or it must be set to zero")
	}
	b.ID = nextBillID
	nextBillID++
	bills = append(bills, &b)
	return b, nil
}

func GetBillByID(id int) (Bill, error) {
	for _, b := range bills {
		if b.ID == id {
			return *b, nil
		}
	}

	return Bill{}, fmt.Errorf("Bill with ID '%v' not found", id)
}

func UpdateBill(b Bill) (Bill, error) {
	for i, candidate := range bills {
		if candidate.ID == b.ID {
			for i, item := range b.Items {
				if item.Quantity > 1 {
					b.Items[i].Total = SumTotal(item.Price, item.Quantity)
					b.Total += b.Items[i].Total
				} else {
					b.Items[i].Total = item.Price
					b.Total += b.Items[i].Total
				}
			}

			if len(b.SplitBetween) > 1 {
				SplitBill(b)
			}

			bills[i] = &b
			return b, nil
		}
	}

	return Bill{}, fmt.Errorf("Bill with ID '%v' not found", b.ID)
}

func AddItemToBill(o Order, billID int) (Bill, error) {
	b, err := GetBillByID(billID)
	if err != nil {
		return Bill{}, err
	}
	var newItem = NewItem(o, b)

	b.Items = append(b.Items, newItem)

	return UpdateBill(b)
}

func SplitBill(b Bill) (Bill, error) {
	remainder := GetRemainder(b)
	quotient, err := Divide(b)
	if err != nil {
		return Bill{}, err
	}

	b = SplitBetweenBuddies(b, quotient)

	if remainder == 0 {
		sendEmailToBuddies(b.SplitBetween)
		return b, nil
	} else {
		var buddy = rand.Intn(len(b.SplitBetween))
		b.SplitBetween[buddy].ShouldPay += remainder
		sendEmailToBuddies(b.SplitBetween)
		return b, nil
	}
}

func sendEmailToBuddies(buddies []Buddy) {
	for _, buddy := range buddies {
		SendEmail(buddy)
	}
}

func RemoveBillByID(id int) error {
	for i, b := range bills {
		if b.ID == id {
			bills = append(bills[:i], bills[i+1:]...)
			return nil
		}
	}

	return fmt.Errorf("Bill with ID '%v' not found", id)
}

func Sum(values ...float64) float64 {
	total := 0.0
	for _, value := range values {
		total += value
	}

	return total
}

func Add(p1, p2 float64) float64 {
	return p1 + p2
}

func Subtract(p1, p2 float64) float64 {
	return p1 - p2
}

func Multiply(p1, p2 float64) float64 {
	return p1 * p2
}
