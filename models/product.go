package models

import (
	"errors"
	"fmt"
)

type Product struct {
	ID    int    `json:"id"`
	Name  string `json:"name" validate:"required"`
	Price int64  `json:"price" validate:"gte=0,required"`
}

var (
	products      []*Product
	nextProductID = 1
)

func GetProducts() []*Product {
	return products
}

func AddProduct(p Product) (Product, error) {
	if p.ID != 0 {
		return Product{}, errors.New("New Product must not include id or it must be set to zero")
	}
	p.ID = nextProductID
	nextProductID++
	products = append(products, &p)

	return p, nil
}

func GetProductByID(id int) (Product, error) {
	for _, p := range products {
		if p.ID == id {
			return *p, nil
		}
	}

	return Product{}, fmt.Errorf("Product with ID '%v' not found", id)
}

func UpdateProduct(p Product) (Product, error) {
	for i, candidate := range products {
		if candidate.ID == p.ID {
			products[i] = &p
			return p, nil
		}
	}

	return Product{}, fmt.Errorf("Product with ID '%v' not found", p.ID)
}

func RemoveProductByID(id int) error {
	for i, p := range products {
		if p.ID == id {
			products = append(products[:i], products[i+1:]...)
			return nil
		}
	}

	return fmt.Errorf("Product with ID '%v' not found", id)
}
