package models

func getTotal(p1, p2 int64) int64 {
	return p1 * p2
}

var (
	nextItemID = 1
)

func NewItem(order Order, bill Bill) Item {
	if len(bill.Items) > 0 {
		nextItemID++
	}

	var newItem = Item{
		ID:          nextItemID,
		ProductName: order.Product.Name,
		Price:       order.Product.Price,
		Total:       order.Total,
		Quantity:    order.Quantity,
	}

	return newItem
}
