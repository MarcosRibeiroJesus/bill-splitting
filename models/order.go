package models

type Order struct { // pedido produto
	ID       int      `json:"id"`
	Product  *Product `json:"product" validate:"required"`
	Quantity int64    `json:"quantity" validate:"gte=1,required"`
	Total    int64    `json:"total"`
	BillID   int      `json:"bill_id" validate:"required"`
}
