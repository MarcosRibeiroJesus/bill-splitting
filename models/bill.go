package models

type Bill struct {
	ID           int     `json:"id"`
	Items        []Item  `json:"items"`
	Total        int64   `json:"total"`
	SplitBetween []Buddy `json:"split_between"`
}
