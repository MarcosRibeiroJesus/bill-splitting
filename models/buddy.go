package models

type Buddy struct {
	Email     string `json:"email"`
	ShouldPay int64  `json:"should_pay"`
}
