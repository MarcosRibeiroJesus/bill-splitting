# Hi there! 

I'm pretty grateful for participating in this process! I would like to have the opportunity to work with you guys on this new project first because I enjoyed everything at Stone, the people, tech stack, challenge, and I believe that it will make me a better professional, surely.

The last week, I've spent time learning Go and seeing if it will be a good fit for me, and I need to say that I loved to try it and see how it is promising.
# Code Challenge: Bill Splitting

First of all, a lot of things in this solution could be better! For example, using concurrency and apply the tests. 

## Introduction - Bill Splitting!

While learning Go through videos on Pluralsight, I created a simple web service based on the Go: Getting Started course and increased the project to fit the requirements of this Code Challenge. I thought of this solution as a Bill Splitting App where we have a Bill and Orders that insert Items inside the Bill. 
At the moment an update on Bill is realized, with a list of emails, a split is processed and the API sends an email for each person in the Buddy Slice which contains the email list and how much they should pay. 

## Run Application

### Prerequisites

To follow the example in this repo, you will need:

A Go workspace set up by following [How To Install Go and Set Up a Local Programming Environment](https://www.digitalocean.com/community/tutorial_series/how-to-install-and-set-up-a-local-programming-environment-for-go).

### Building Go Binaries With go build

Using go build, you can generate an executable binary for our Go application, allowing you to distribute and deploy the program where you want.

Try this with main.go. In our Bill-Splitting directory, run the following command:

`go build`

If you do not provide an argument to this command, go build will automatically compile the main.go program in your current directory. The command will include all your \*.go files in the directory. It will also build all of the supporting code needed to be able to execute the binary on any computer with the same system architecture, regardless of whether that system has the .go source files, or even a Go installation.

In this case, you built your Bill-Splitting application into an executable file that was added to your current directory. Check this by running the ls command:

`ls`

If you are running macOS or Linux, you will find a new executable file that has been named after the directory in which you built your program:

`controllers `**`Bill-Splitting`**` go.mod go.sum main.go models README.md`

Note: On Windows, your executable will be **Bill-Splitting.exe**.

By default go build will generate an executable for the current platform and architecture. For example, if built on a linux/386 system, the executable will be compatible with any other linux/386 system, even if Go is not installed. Go supports building for other platforms and architectures, which you can read more about in our Building Go Applications for Different Operating Systems and Architectures article.

Now, that you�ve created your executable, run it to make sure the binary has been built correctly. On macOS or Linux, run the following command:

`./Bill-Splitting`

On Windows, run:

`Bill-Splitting.exe`

Now you can go to the postman and test our endpoints in the Postman link above:

*To Facilitate the test of the solution: 
Go directly to the folder Bill, and create a new Bill using the **POST New** request with a empty Body '{}' that will create a object like the following:

> controllers > bill.go > (billController).post |
> models > bill.service.go > AddBill 

```
{
    "id": 1,
    "items": null,
    "total": 0,
    "split_between": null
}
```
Then, in the same folder update the created Bill using the **PUT :id** request with the following Body: 

> controllers > bill.go > (billController).put |
> models > bill.service.go > UpdateBill 

```
{
    "id": 1,
    "items": [
        {
            "id": 1,
            "product_name": "iphone 12",
            "price": 1200005,
            "total": 2400000,
            "quantity": 2
        },
        {
            "id": 2,
            "product_name": "Samsung S21",
            "price": 607007,
            "total": 1200000,
            "quantity": 2
        },
        {
            "id": 3,
            "product_name": "Moto G100",
            "price": 325407,
            "total": 976221,
            "quantity": 4
        },
        {
            "id": 4,
            "product_name": "iphone 12",
            "price": 559909,
            "total": 167970,
            "quantity": 3
        }
    ],
    "total": 24000000,
    "split_between": [
        {
            "email": "your_email_1@gmail.com"
        },
        {
            "email": "your_email_2@outlook.com"
        },
        {
            "email": "your_email_3@gmail.com"
        }
    ]
}
```
That will return the following JSON that represents a CLOSED Bill:

```
{
    "id": 1,
    "items": [
        {
            "id": 1,
            "product_name": "iphone 12",
            "price": 1200005,
            "total": 2400010,
            "quantity": 2
        },
        {
            "id": 2,
            "product_name": "Samsung S21",
            "price": 607007,
            "total": 1214014,
            "quantity": 2
        },
        {
            "id": 3,
            "product_name": "Moto G100",
            "price": 325407,
            "total": 1301628,
            "quantity": 4
        },
        {
            "id": 4,
            "product_name": "iphone 12",
            "price": 559909,
            "total": 1679727,
            "quantity": 3
        }
    ],
    "total": 30595379,
    "split_between": [
        {
            "email": "marcoz.iu@gmail.com",
            "should_pay": 10198459
        },
        {
            "email": "mrj84@outlook.com",
            "should_pay": 10198459
        },
        {
            "email": "mrj.aws.2021@gmail.com",
            "should_pay": 10198461
        }
    ]
}
```

[Postman requests](https://www.postman.com/mrj84/workspace/go-language/collection/1830326-dd360df1-73af-46f7-b635-56cbb51ee1fa?ctx=documentation)
